<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class LoginTest extends TestCase
{
    public function testShowLoginForm()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    public function testRegisterNonExistingUser()
    {
        $response = $this->post('/login', [
            'email' => 'new_user@localhost.me',
            'password' => 'secret'
        ]);

        $user = User::whereEmail('new_user@localhost.me')->first();

        $this->assertNotNull($user);
        $this->assertFalse($user->hasVerifiedEmail());
        $this->assertAuthenticatedAs($user);

        $response->assertLocation('/');
    }

    public function testLoginExistingUser()
    {
        $user = factory(User::class)->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $this->assertAuthenticatedAs($user);
        $response->assertLocation('/');
    }

    public function testRevokeInvalidLoginAttempt()
    {
        $user = factory(User::class)->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'wrong password'
        ]);

        $this->assertGuest();
        $response->assertSessionHasErrors(['email']);
    }
}
