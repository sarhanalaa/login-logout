<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class HomeTest extends TestCase
{
    public function testRedirectUnAuthenticatedRequestToLogin()
    {
        $response = $this->get('/');

        $this->assertGuest();
        $response->assertLocation('/login');
    }

    public function testRenderHome()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/');

        $response->assertSuccessful();
        $response->assertViewIs('home');
    }

    /**
     * @dataProvider showUserStatusData
     */
    public function testShowUserStatus(
        $user,
        $expectedVerificationText
    ) {
        $response = $this->actingAs($user)->get('/');

        $response->assertSee($user->id);
        $response->assertSee($expectedVerificationText);
    }

    public function showUserStatusData()
    {
        $this->refreshApplication();

        $user = factory(User::class)->create();
        $unverifiedUser = factory(User::class)->states(['unverified'])->create();

        return [
            [$user, 'You have been successfully verified.'],
            [$unverifiedUser, 'You have not yet verified your email address.']
        ];
    }
}
