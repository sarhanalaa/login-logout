<?php

namespace App\Http\Controllers\Auth;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (
            $this->attemptLogin($request)
            || $this->attemptRegister($request)
        ) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|email',
            'password' => 'required|string|min:5',
        ]);
    }

    protected function attemptRegister(Request $request)
    {
        $credentials = $this->credentials($request);

        if (User::whereEmail($credentials[$this->username()])->count()) {
            return false;
        }

        $user = User::create([
            'email' => $credentials['email'],
            'password' => Hash::make($credentials['password']),
        ]);

        event(new Registered($user));

        $this->guard()->login($user);

        return true;
    }
}
