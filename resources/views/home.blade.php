@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Hey {{ Auth::user()->email }}</div>

                <div class="card-body">
                    <p>
                        Your user id is <strong>{{ Auth::user()->id }}</strong>
                    </p>
                    <p>
                    @if(Auth::user()->email_verified_at)
                        You have been successfully verified. Have fun!
                    @else
                        You have not yet verified your email address.
                        Please check your inbox for a verification email!
                    @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
